package zw.co.ift.creditrating.banking.core.rest.implementation

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyExtractors
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import zw.co.ift.creditrating.banking.core.rest.BankService
import zw.co.ift.creditrating.banking.core.rest.errors.ErrorMessage
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.model.AccountTransaction
import zw.co.ift.creditrating.persistence.service.AccountService
import zw.co.ift.creditrating.persistence.service.TransactionService
import zw.co.ift.creditrating.utilities.generateAccountNumber
import java.net.URI
import java.time.Duration

/**
 * used to handle the requests you get for the bank
 */
@Service
class BankServiceHandler(private val accountService: AccountService, private val transactionService: TransactionService) : BankService {

    val log: Logger = LoggerFactory.getLogger(BankServiceHandler::class.java)

    override fun transactionSearch(serverRequest: ServerRequest): Mono<ServerResponse> {
        val transactionReference = serverRequest.pathVariable("transactionId")
        log.info("Searching for transaction reference :: $transactionReference")
        val transactionMono = transactionService.findByReference(transactionReference)
        log.info("Retrieved transaction $transactionMono")
        return transactionMono
                .log()
                .flatMap { transaction -> ServerResponse.status(HttpStatus.FOUND).contentType(APPLICATION_JSON_UTF8).body(fromObject(transaction)) }
                .switchIfEmpty(ServerResponse.notFound().build())
    }

    /**
     *  searches for an account in the banking system
     */
    override fun accountSearch(serverRequest: ServerRequest): Mono<ServerResponse> {
        val accountNumber = serverRequest.pathVariable("accountNumber")
        log.info("Searching for account number $accountNumber")
        val accountMono = accountService.findByAccountNumber(accountNumber)
        log.info("Retrieved account $accountMono")
        return accountMono
                .log()
                .flatMap { account -> ServerResponse.status(HttpStatus.FOUND).contentType(APPLICATION_JSON_UTF8).body(fromObject(account)) }
                .switchIfEmpty(ServerResponse.notFound().build())
    }

    /**
     * an account should be created in under 10 seconds of processing
     * failing which a timeout exception is raised
     */
    override fun createAccount(serverRequest: ServerRequest): Mono<ServerResponse> {
        var accountNumber: String = generateAccountNumber()
        log.info("Processing account creation ...Account $accountNumber")
        return serverRequest.body(BodyExtractors.toMono(Account::class.java))
                .log()
                .timeout(Duration.ofSeconds(10))
                .flatMap({ account -> accountService.createAccount(account) })
                .flatMap { createdAccount -> ServerResponse.created(URI.create("/api/account/${createdAccount.accountNumber}")).contentType(APPLICATION_JSON_UTF8).body(fromObject(createdAccount)) }
                .switchIfEmpty(ServerResponse.badRequest().contentType(APPLICATION_JSON_UTF8).body(fromObject(ErrorMessage("ERR.001", "Account already exists in the system"))))
    }

    override fun createTransaction(serverRequest: ServerRequest): Mono<ServerResponse> {
        log.info("Processing a new transaction ...")
        return serverRequest.body(BodyExtractors.toMono(AccountTransaction::class.java))
                .log()
                .timeout(Duration.ofSeconds(10))
                .flatMap { accountTransaction -> transactionService.processTransaction(accountTransaction) }
                .flatMap { processedTransaction -> ServerResponse.created(URI.create("/api/transaction/info/${processedTransaction.reference}")).contentType(APPLICATION_JSON_UTF8).body(fromObject(processedTransaction)) }
                .switchIfEmpty(ServerResponse.badRequest().contentType(APPLICATION_JSON_UTF8).body(fromObject(ErrorMessage("ERR.002", "Transaction processing failed"))))
    }

}