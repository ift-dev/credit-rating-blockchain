package zw.co.ift.creditrating.banking.core.rest

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

interface BankService {
    fun accountSearch(serverRequest: ServerRequest): Mono<ServerResponse>
    fun createAccount(serverRequest: ServerRequest): Mono<ServerResponse>
    fun transactionSearch(serverRequest: ServerRequest): Mono<ServerResponse>
    fun createTransaction(serverRequest: ServerRequest): Mono<ServerResponse>

}

