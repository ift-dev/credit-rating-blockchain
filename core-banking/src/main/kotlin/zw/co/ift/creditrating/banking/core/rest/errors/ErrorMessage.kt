package zw.co.ift.creditrating.banking.core.rest.errors

data class ErrorMessage(val code: String, val message: String)
