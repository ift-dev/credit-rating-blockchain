package zw.co.ift.creditrating.banking.core

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication

class CoreBanking

fun main(args: Array<String>) {
    SpringApplication.run(CoreBanking::class.java, *args)
}