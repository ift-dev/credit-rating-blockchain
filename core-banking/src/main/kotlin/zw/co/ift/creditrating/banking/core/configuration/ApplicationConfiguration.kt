package zw.co.ift.creditrating.banking.core.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.function.server.router
import zw.co.ift.creditrating.banking.core.rest.BankService
import zw.co.ift.creditrating.persistence.configuration.PersistenceConfiguration

@Configuration
@ComponentScan(basePackages = ["zw.co.ift"])
@EnableWebFlux
//@EnableDiscoveryClient
@Import(value = [(PersistenceConfiguration::class)])
class CoreBankingConfiguration {
    @Bean
    fun apiRouter(bankService: BankService) = router {
        (accept(MediaType.APPLICATION_JSON) and "/api").nest {
            "/account".nest {
                GET("/info/{accountNumber}", bankService::accountSearch)
                POST("/create", bankService::createAccount)
            }
            "/transaction".nest {
                GET("/info/{transactionId}", bankService::transactionSearch)
                POST("/create", bankService::createTransaction)
            }
        }
    }
}

