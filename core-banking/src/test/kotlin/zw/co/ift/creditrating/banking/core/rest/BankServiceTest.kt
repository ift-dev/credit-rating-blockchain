package zw.co.ift.creditrating.banking.core.rest

import de.flapdoodle.embed.mongo.MongodExecutable
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.model.AccountCategory
import zw.co.ift.creditrating.persistence.model.AccountTransaction
import zw.co.ift.creditrating.persistence.model.TransactionState
import zw.co.ift.creditrating.persistence.service.AccountService
import zw.co.ift.creditrating.persistence.service.TransactionService
import java.math.BigDecimal
import java.time.LocalDateTime

@WebFluxTest
@ActiveProfiles("test")
@ContextConfiguration(classes = [(BankServiceTestConfiguration::class)])
@RunWith(SpringRunner::class)
class BankServiceTest {
    private val log: Logger = LoggerFactory.getLogger(BankServiceTest::class.java)

    private val savingsAccount = AccountCategory(id = "1", maximumBalance = BigDecimal.TEN, minimumBalance = BigDecimal.ONE, name = "Savings Account")
    private val sampleAccount = Account(id = "1", accountName = "Developer Account", accountNumber = "ACC.0001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE)
    private val sampleAccountTest = Account(id = "1", accountName = "Developer", accountNumber = "00001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE)
    private val sampleTransactionId = "TRANS.00001"
    private val sampleTransaction = AccountTransaction(sourceAccount = "ACC.00001", destinationAccount = "ACC.00002", amount = BigDecimal.ONE, reference = sampleTransactionId, state = TransactionState.SUCCESSFUL, transactionTime = LocalDateTime.now(), description = "Completed successfully", transactionCategory = "TRANSFER")
    private val samplePendingTransaction = AccountTransaction(sourceAccount = "ACC.00001", destinationAccount = "ACC.00002", amount = BigDecimal.ONE, state = TransactionState.PENDING, transactionTime = LocalDateTime.now(), description = "Funds transfer", transactionCategory = "TRANSFER")
    @Autowired
    private lateinit var apiRouter: RouterFunction<ServerResponse>

    private lateinit var webClient: WebTestClient

    @MockBean
    private lateinit var accountService: AccountService
    @MockBean
    private lateinit var transactionService: TransactionService

    companion object {
        private lateinit var starter: MongodStarter
        private lateinit var mongodExecutable: MongodExecutable
        /**
         * work around as the persistence module is requiring access to mongodb
         */
        @BeforeClass
        @JvmStatic
        fun initializeEmbeddedMongo() {
            starter = MongodStarter.getDefaultInstance()
            val mongodConfig = MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(Net(27017, Network.localhostIsIPv6()))
                    .build()
            mongodExecutable = starter.prepare(mongodConfig)
            mongodExecutable.start()
        }

        @AfterClass
        @JvmStatic
        fun closeDatabase() = mongodExecutable.stop()
    }

    @Before
    fun initializeWebClient() {
        webClient = WebTestClient.bindToRouterFunction(apiRouter).build()
    }

    /**
     * Positive test to search for an existing account
     */
    @Test
    fun `search for an existing acccount`() {
        `when`(accountService.findByAccountNumber("ACC.0001")).thenReturn(sampleAccount.toMono())
        val response = webClient
                .get()
                .uri("/api/account/info/ACC.0001")
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isFound
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.accountNumber").isNotEmpty
                .jsonPath("$.accountName").isEqualTo("Developer Account")
                .returnResult()
        log.info("Received response :: $response")
    }

    @Test
    fun `search for non existing acccount`() {
        log.info("Testing the search for an accoun that does not exist ")
        `when`(accountService.findByAccountNumber("ACC.0001")).thenReturn(Mono.empty())
        val response = webClient
                .get()
                .uri("/api/account/info/ACC.0001")
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .returnResult()
        log.info("Received response :: $response")
    }

    /**
     * this tests the API and checks if it returns the right elements
     */
    @Test
    fun `create a new account`() {
        log.info("Testing the creation of a new account")
        `when`(accountService.createAccount(sampleAccount)).thenReturn(sampleAccountTest.toMono())
        val response = webClient
                .post()
                .uri("/api/account/create")
                .accept(APPLICATION_JSON_UTF8)
                .contentType(APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(sampleAccount))
                .exchange()
                .expectStatus().isCreated
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.accountNumber").isNotEmpty
                .jsonPath("$.accountNumber").isEqualTo("00001")
                .jsonPath("$.accountName").isEqualTo("Developer")
                .returnResult()
        log.info("Received response :: $response")
    }

    @Test
    fun `create an existing account`() {
        log.info("Testing to create an account with an already saved account number")
        `when`(accountService.createAccount(sampleAccount)).thenReturn(Mono.empty())
        val response = webClient
                .post()
                .uri("/api/account/create")
                .accept(APPLICATION_JSON_UTF8)
                .contentType(APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(sampleAccount))
                .exchange()
                .expectStatus().isBadRequest
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.code").isEqualTo("ERR.001")
                .jsonPath("$.message").isEqualTo("Account already exists in the system")
                .returnResult()
        log.info("Received response :: $response")
    }

    @Test
    fun `search for an existing transaction`() {
        log.info("Searching for an existing transaction")
        `when`(transactionService.findByReference(sampleTransactionId)).thenReturn(sampleTransaction.toMono())
        val response = webClient
                .get()
                .uri("/api/transaction/info/$sampleTransactionId")
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isFound
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.reference").isEqualTo(sampleTransactionId)
                .jsonPath("$.sourceAccount").isEqualTo("ACC.00001")
                .returnResult()
        log.info("Received response :: $response")
    }

    @Test
    fun `search for a non existing transaction`() {
        log.info("Searching for an existing transaction")
        `when`(transactionService.findByReference("TRANS.00001002")).thenReturn(Mono.empty())
        val response = webClient
                .get()
                .uri("/api/transaction/info/TRANS.00001002")
                .accept(APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .returnResult()
        log.info("Received response :: $response")
    }


    @Test
    fun `process a transaction`() {
        log.info("Creating a new transaction ...")
        `when`(transactionService.processTransaction(samplePendingTransaction)).thenReturn(sampleTransaction.toMono())
        val response = webClient
                .post()
                .uri("/api/transaction/create")
                .accept(APPLICATION_JSON_UTF8)
                .contentType(APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(samplePendingTransaction))
                .exchange()
                .expectStatus().isCreated
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.sourceAccount").isEqualTo("ACC.00001")
                .jsonPath("$.destinationAccount").isEqualTo("ACC.00002")
                .jsonPath("$.reference").isNotEmpty
                .jsonPath("$.state").isEqualTo(TransactionState.SUCCESSFUL.name)
                .returnResult()
        log.info("Received response :: $response")
    }

    @Test
    fun `processing a failing transaction`() {
        log.info("Creating a new transaction ...")
        `when`(transactionService.processTransaction(samplePendingTransaction)).thenReturn(Mono.empty())
        val response = webClient
                .post()
                .uri("/api/transaction/create")
                .accept(APPLICATION_JSON_UTF8)
                .contentType(APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(samplePendingTransaction))
                .exchange()
                .expectStatus().isBadRequest
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.code").isEqualTo("ERR.002")
                .jsonPath("$.message").isEqualTo("Transaction processing failed")
                .returnResult()
        log.info("Received response :: $response")
    }
/*


    @Test
    fun `update an account`() {

    }*/
}