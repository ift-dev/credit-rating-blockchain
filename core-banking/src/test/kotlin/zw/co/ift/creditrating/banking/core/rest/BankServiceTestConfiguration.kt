package zw.co.ift.creditrating.banking.core.rest

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["zw.co.ift.creditrating.banking.core"])
class BankServiceTestConfiguration