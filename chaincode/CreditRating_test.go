package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
	"time"
)

const (
	TestSourceAccount      = "ACC.001"
	TestDestinationAccount = "ACC.002"
	NoneExistingAccount    = "SOME_ACCOUNT_NUMBER"
)

/**
Tests the initialization of the chaincode
*/
func TestCreditRating_Init(t *testing.T) {
	creditRatingContract := new(CreditRating)
	stub := shim.NewMockStub("mockstub", creditRatingContract)
	testContractInit(t, stub, [][]byte{})
}

/**
Used to test the functions offered by the chaincode
*/
func TestCreditRating_Invoke(t *testing.T) {
	creditRatingContract := new(CreditRating)
	stub := shim.NewMockStub("mockstub", creditRatingContract)
	testGettingValidAccount(t, stub)
	testGettingInvalidAccount(t, stub)
	testAccountSearchNoAccountSupplied(t, stub)
	testAddTransactionToNoneExistingAccount(t, stub)
}

/**
  test contract initialization
*/
func testContractInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	log.Println("- Testing for smart contract intitiation")
	response := stub.MockInit("1", args)
	if response.Status != shim.OK {
		log.Println("- Initialization of chaincode failed :: ", string(response.Message))
		t.FailNow()
	}
	log.Println(fmt.Sprintf("- Response :: Status : %d :: Message => %s", response.Status, response.Message))
	assert.Equal(t, int32(200), response.Status, "Response is expected to be 200 for successful initialization")
	fmt.Println("===========================================")
}

/**
test getting account details for avalid account
*/
func testGettingValidAccount(t *testing.T, stub *shim.MockStub) {
	log.Println("- Testing valid account search...")
	if createTestAccount(t, stub) {
		response := stub.MockInvoke("TXT.001", [][]byte{[]byte("accountSearch"), []byte(TestSourceAccount)})
		log.Println(fmt.Sprintf("- Response :: Status : %d :: Message => %s", response.Status, response.Message))
		account := Account{}
		json.Unmarshal(response.Payload, &account)
		assert.Equal(t, int32(200), response.Status, "Invocation failed")
		assert.Equal(t, TestSourceAccount, account.AccountNumber, "Failed to retrieve the correct account")
	} else {
		log.Fatal("- Failed to create the required test account")
		t.FailNow()
	}
	fmt.Println("===========================================")
}

/**
test getting account details for a non-existing account
*/
func testGettingInvalidAccount(t *testing.T, stub *shim.MockStub) {
	log.Println("- Testing invalid account search...")
	response := stub.MockInvoke("TXT.001", [][]byte{[]byte("accountSearch"), []byte(NoneExistingAccount)})
	log.Println(fmt.Sprintf("-Response :: Status : %d :: Message => %s", response.Status, response.Message))
	account := Account{}
	json.Unmarshal(response.Payload, &account)
	assert.Equal(t, int32(500), response.Status, fmt.Sprintf("Account %s has been found on the blockchain", NoneExistingAccount))
	assert.NotEqual(t, NoneExistingAccount, account.AccountNumber, fmt.Sprintf("Specified account : %s exists in the blockchain", NoneExistingAccount))
	assert.Equal(t, fmt.Sprintf("Requested account [%s] does not exist", NoneExistingAccount), response.Message, "The response message is invalid")
	fmt.Println("===========================================")
}

/**
test trying to get an account without supplying an account number
*/
func testAccountSearchNoAccountSupplied(t *testing.T, stub *shim.MockStub) {
	log.Println("- Testing none existing account search...")
	response := stub.MockInvoke("TXT.001", [][]byte{[]byte("accountSearch")})
	log.Println(fmt.Sprintf("- Response :: Status : %d :: Message => %s", response.Status, response.Message))
	account := Account{}
	json.Unmarshal(response.Payload, &account)
	assert.Equal(t, int32(500), response.Status, fmt.Sprintf("Account %s has been found on the blockchain", NoneExistingAccount))
	assert.Equal(t, "Expected 1 argument to search for an account but received 0", response.Message, "The expected message is invalid")
	fmt.Println("===========================================")
}

/**
test adding an account to a non-existant account
this should result in the creation of the specified account
*/
func testAddTransactionToNoneExistingAccount(t *testing.T, stub *shim.MockStub) {
	log.Println("- Testing successful transaction addition...")
	response := stub.MockInvoke("2", [][]byte{[]byte("addTransaction"), []byte("Funds Transfer"), []byte(TestSourceAccount), []byte(TestDestinationAccount), []byte("5.0"), []byte(COMPLETED), []byte("REF.001")})
	log.Println(fmt.Sprintf("- Response :: Status : %d :: Message => %s", response.Status, response.Message))
	assert.Equal(t, int32(200), response.Status, "HTTP status 200 is expected ")
	log.Printf("- Added Transaction %s to account %s", "TXN.001", TestSourceAccount)
	savedAccountAsBytes, _ := stub.GetState(TestSourceAccount)
	assert.NotNil(t, savedAccountAsBytes)
	account := Account{}
	json.Unmarshal(savedAccountAsBytes, &account)
	log.Printf("- Current Rating %f", account.Rating)
	log.Printf("-  account : %v", account)
	assert.Equal(t, TestSourceAccount, account.AccountNumber, "Failed to create an account on intial transaction addition")
}

func createTestAccount(t *testing.T, stub *shim.MockStub) bool {
	log.Println("- Creating test account...")
	ratingTransaction := RatingTransaction{
		TransactionTime:    time.Now(),
		Amount:             1.0,
		DestinationAccount: TestDestinationAccount,
		SourceAccount:      TestSourceAccount,
		TransactionType:    fundsTransfer,
		Reference:          "TXN.001",
	}
	account := Account{AccountNumber: ratingTransaction.SourceAccount, CreatedDate: time.Now(), State: ACTIVE, Transactions: []RatingTransaction{ratingTransaction}}
	accountJson, _ := json.Marshal(account)
	stub.MockTransactionStart("TXX.001")
	saveError := stub.PutState("ACC.001", accountJson)
	stub.MockTransactionEnd("TXX.001")
	response := saveError == nil
	log.Printf("- Test account created : %v\n", response)
	return response
}
