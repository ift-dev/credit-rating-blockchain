package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"log"
	"reflect"
	"strconv"
	"time"
)

type CreditRating struct {
}

/**
used to host the actual points decision table
*/
type RatingPoint struct {
	event  string
	points float64
	state  bool
}

/**
defining transaction types
*/
const (
	// transaction states
	balanceEnquiry        = "BalanceEnquiry"
	fundsTransfer         = "FundsTransfer"
	goodPurchase          = "GoodPurchase"
	badPurchase           = "BadPurchase"
	savingsDeposit        = "SavingsDeposit"
	loanMonthlyDefault    = "LoanMonthlyDefault"
	loanMonthlyRepayment  = "LoanMonthlyRepayment"
	loanSuccessfulClosure = "LoanSuccessfulClosure"
	loanBadDebt           = "LoanBadDebt"
	//Account and transaction states
	ACTIVE    = "ACTIVE"
	COMPLETED = "COMPLETED"
)

/**
define the Rating point constants as a map
this is for a simple case. In a more robust solution this would make a call to a point service and return back the
*/

var ratingPoints = map[string]RatingPoint{
	balanceEnquiry:        {event: balanceEnquiry, points: 0.5, state: true},
	goodPurchase:          {event: goodPurchase, points: 3.0, state: true},
	fundsTransfer:         {event: goodPurchase, points: 3.0, state: true},
	badPurchase:           {event: badPurchase, points: -3.0, state: true},
	savingsDeposit:        {event: savingsDeposit, points: 5.0, state: true},
	loanMonthlyDefault:    {event: loanMonthlyDefault, points: -10.0, state: true},
	loanMonthlyRepayment:  {event: loanMonthlyRepayment, points: 15.0, state: true},
	loanSuccessfulClosure: {event: loanSuccessfulClosure, points: 3.0, state: true},
	loanBadDebt:           {event: loanBadDebt, points: -9.0, state: true},
}

/**
provides the Rating State of a transaction
*/
type Account struct {
	AccountNumber string
	Rating        float64
	CreatedDate   time.Time
	State         string
	Transactions  []RatingTransaction
}

/**
records the Transactions that will have taken place
*/
type RatingTransaction struct {
	TransactionType    string
	SourceAccount      string
	DestinationAccount string
	Amount             float64
	State              string
	TransactionTime    time.Time
	RatingScore        RatingPoint
	Reference          string
}

/**
called when the chaincode is deployed on the network for the first time
*/
func (creditRating *CreditRating) Init(stub shim.ChaincodeStubInterface) pb.Response {
	log.Println("- Initializing the CreditRating chaincode ...")
	return shim.Success(nil)
}

/**
used to retrieve all the existing Rating scores in the system
*/ /*
func (creditRating *CreditRating) getRatingPoints(stub shim.ChaincodeStubInterface) pb.Response {


}*/

/**
used to retrieve all the existing Rating scores in the system
expected arguments :
 0 -> TransactionType  : See Rating points for possible events
 1 -> source account
 2 -> destination account
 3 -> Amount : this is a float64
 4 -> State : only Transactions that have State completed are to be processed
 5 -> Reference unique key that identifies the transaction
also note only active accounts shall be processed. If an account is deactivated or barred it shall not be processed
*/
func (creditRating *CreditRating) addTransaction(stub shim.ChaincodeStubInterface, arguments []string) pb.Response {
	log.Printf("-  Processing transaction addition ...")
	if len(arguments) != 6 {
		return shim.Error(fmt.Sprintf("Expected 5 arguments to add transaction but received %x", len(arguments)))
	}
	transactionAmount, floatParseError := strconv.ParseFloat(arguments[3], 64)
	if floatParseError != nil {
		return shim.Error(fmt.Sprintf("%s is not a valid Amount", arguments[3]))
	}
	transactionType, notFound := ratingPoints[arguments[0]]
	if notFound {
		allowedTransactions := reflect.ValueOf(ratingPoints).MapKeys()
		return shim.Error(fmt.Sprintf("The specified transaction type [%s] is currently not supported. Supported transactions : %s", arguments[0], allowedTransactions))
	}
	if COMPLETED == arguments[4] {
		account := Account{}
		ratingTransaction := RatingTransaction{
			TransactionTime:    time.Now(),
			Amount:             transactionAmount,
			DestinationAccount: arguments[2],
			SourceAccount:      arguments[1],
			TransactionType:    transactionType.event,
			Reference:          arguments[5],
		}
		accountAsBytes, getAccountStateError := stub.GetState(ratingTransaction.SourceAccount)
		if getAccountStateError != nil {
			// we dont have an existing account lets create the account and then process
			log.Println(fmt.Sprintf("- New account received %s", ratingTransaction.SourceAccount))
			account = Account{AccountNumber: ratingTransaction.SourceAccount, CreatedDate: time.Now(), State: ACTIVE, Transactions: []RatingTransaction{ratingTransaction}}
			accountJson, _ := json.Marshal(account)
			stub.PutState(ratingTransaction.SourceAccount, accountJson)
			accountAsBytes, getAccountStateError = stub.GetState(ratingTransaction.SourceAccount)
		}
		json.Unmarshal(accountAsBytes, &account)
		// we now possess the account
		accountResponse, accountProcessingError := processTransaction(account, ratingTransaction)
		if accountProcessingError == nil {
			accountProcessedAsBytes, _ := json.Marshal(accountResponse)
			updateAccountError := stub.PutState(accountResponse.AccountNumber, accountProcessedAsBytes)
			if updateAccountError != nil {
				return shim.Error(updateAccountError.Error())
			}
			log.Println("- Processed account update (success)")
			return shim.Success(nil)
		} else {
			return shim.Error(accountProcessingError.Error())
		}
	} else {
		return shim.Error(fmt.Sprintf("Your transaction is marked as %s. Only COMPLETED Transactions can be processed", arguments[4]))
	}
}

/**
computes the score for an account
we calculate the cumulative score and then get an average given the number of Transactions
*/
func compute(account Account) float64 {
	log.Printf("- Computing Rating score for %s", account.AccountNumber)
	elements := len(account.Transactions)
	log.Println(elements)
	ratingSum := 0.0
	if account.State == ACTIVE {
		for countIndex := range account.Transactions {
			log.Printf("- Adding new rating of %f for %v", account.Transactions[countIndex].RatingScore.points,account.Transactions[countIndex])
			ratingSum += account.Transactions[countIndex].RatingScore.points
		}
		return ratingSum / float64(elements)
	}
	return 0.0
}

func processTransaction(account Account, transaction RatingTransaction) (Account, error) {
	log.Printf("- Appending transaction %v to %s", transaction.Reference, account.AccountNumber)
	if account.State == ACTIVE {
		account.Transactions = append(account.Transactions, transaction)
		account.Rating = compute(account)
		log.Printf("- Account [%s] rating : %f", account.AccountNumber , account.Rating)
		return account, nil
	}
	return Account{}, errors.New("an account must be in the active State to be processed")
}

/**
 used to retrieve Rating for an account
- arguments :
   0 -> AccountNumber
*/
func (creditRating *CreditRating) accountSearch(stub shim.ChaincodeStubInterface, arguments []string) pb.Response {
	if len(arguments) != 1 {
		return shim.Error(fmt.Sprintf("Expected 1 argument to search for an account but received %x", len(arguments)))
	}
	log.Printf("- Searching for account : %s\n", arguments[0])
	accountNumber := arguments[0]
	accountAsBytes, accountQueryError := stub.GetState(accountNumber)
	if accountQueryError != nil {
		return shim.Error(fmt.Sprintf("Failed to find the requested account [%s]", accountNumber))
	}
	account := Account{}
	json.Unmarshal(accountAsBytes, &account)
	if account.AccountNumber == "" {
		log.Printf("- Failed to find account [%s]\n", accountNumber)
		return shim.Error(fmt.Sprintf("Requested account [%s] does not exist", accountNumber))
	}
	log.Printf("- Retreived account [%s]\n", account.AccountNumber)
	return shim.Success(accountAsBytes)
}

/**
we use this method to determine the possible loan application that can be given to an individual
*/
func (creditRating *CreditRating) getApplicableLoan(stub shim.ChaincodeStubInterface, arguments []string) pb.Response {
	if len(arguments) != 1 {
		return shim.Error(fmt.Sprintf("Expected 1 argument to add transaction but received %x", len(arguments)))
	}
	// first we get the current credit Rating for the client
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "CreditRating"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (creditRating *CreditRating) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, arguments := stub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	switch function {
	case "addTransaction":
		return creditRating.addTransaction(stub, arguments)
	case "accountSearch":
		return creditRating.accountSearch(stub, arguments)
	case "getApplicableLoanAmount":
		return creditRating.getApplicableLoan(stub, arguments)
	}
	return pb.Response{}
}
func main() {
	startUpError := shim.Start(new(CreditRating))
	if startUpError != nil {
		log.Fatalf("- Error starting the Credit Rating service : %s\n", startUpError)
	}
}
