package zw.co.ift.creditrating.utilities

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

fun generateAccountNumber(): String {
    val currentDate = DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now()).toString()
    val accountNumber = Regex("[^0-9 ]").replace(currentDate, "")
    val suffix = (1000..9999).random()
    return "$accountNumber$suffix"
}

fun generateTransactionReference(): String {
    val currentTimestamp = LocalDateTime.now()
    val currentDateOfYear = currentTimestamp.dayOfYear
    val currentTime = DateTimeFormatter.ISO_LOCAL_TIME.format(currentTimestamp)
    val time = Regex("[^0-9 ]").replace(currentTime.toString(), "")
    val suffix = (1000..9999).random()
    return "$currentDateOfYear$time$suffix"
}

fun ClosedRange<Int>.random() = Random().nextInt((endInclusive + 1) - start) + start
