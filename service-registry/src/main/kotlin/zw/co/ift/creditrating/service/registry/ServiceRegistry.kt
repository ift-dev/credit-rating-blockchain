package zw.co.ift.creditrating.service.registry

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@EnableEurekaServer
@SpringBootApplication
class ServiceRegistry

fun main(args: Array<String>) {
    SpringApplication.run(ServiceRegistry::class.java, *args)
}
