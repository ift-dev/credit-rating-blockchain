package zw.co.ift.creditrating.messaging.configuration

import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@EnableRabbit
@ComponentScan(basePackages = ["zw.co.ift.creditrating.messaging"])
class MessagingConfiguration {

    @Bean
    fun messageReceiver() = Queue("blockchain-receiver")
}