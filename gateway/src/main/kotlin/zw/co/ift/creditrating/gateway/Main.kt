package zw.co.ift.creditrating.gateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@EnableDiscoveryClient
@SpringBootApplication
class Main
fun main(args: Array<String>) {

}