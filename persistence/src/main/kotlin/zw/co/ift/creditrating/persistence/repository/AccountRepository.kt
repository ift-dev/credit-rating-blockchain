package zw.co.ift.creditrating.persistence.repository

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Mono
import zw.co.ift.creditrating.persistence.model.Account

interface AccountRepository : ReactiveMongoRepository<Account, String> {
    fun findByAccountNumber(accountNumber: String): Mono<Account>
}