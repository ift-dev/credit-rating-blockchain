package zw.co.ift.creditrating.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import zw.co.ift.creditrating.utilities.generateAccountNumber
import java.math.BigDecimal
import java.time.LocalDateTime

@Document
data class Account(
        @Id
        var id: String? = null,
        @Indexed(unique = true)
        var accountNumber: String = generateAccountNumber(),
        var creationDate: LocalDateTime = LocalDateTime.now(),
        var accountName: String,
        var accountCategory: AccountCategory,
        var currentBalance: BigDecimal,
        var accountState: String
)