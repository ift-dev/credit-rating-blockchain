package zw.co.ift.creditrating.persistence.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.repository.AccountRepository
import zw.co.ift.creditrating.utilities.generateAccountNumber
import java.time.Duration

interface AccountService {
    fun findByAccountNumber(accountNumber: String): Mono<Account>
    fun createAccount(account: Account): Mono<Account>
    fun update(source: Account): Mono<Account>
}


@Service
class AccountServiceImpl(private val repository: AccountRepository) : AccountService {

    val log: Logger = LoggerFactory.getLogger(AccountServiceImpl::class.java)

    override fun createAccount(account: Account): Mono<Account> {
        log.info("Processing new accont request")
        return findByAccountNumber(account.accountNumber)
                .log()
                .timeout(Duration.ofSeconds(10))
                .flatMap({ existingAccount -> existingAccount.toMono() })
                .switchIfEmpty(createNewAccount(account))

    }

    private fun createNewAccount(account: Account): Mono<Account> {
        log.info("Creating a new account for ${account.accountName}")
        account.accountNumber = generateAccountNumber()
        return repository.save(account)
    }

    override fun findByAccountNumber(accountNumber: String): Mono<Account> = repository.findByAccountNumber(accountNumber)

    override fun update(account: Account): Mono<Account> = repository.save(account)

}


