package zw.co.ift.creditrating.persistence.repository

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Mono
import zw.co.ift.creditrating.persistence.model.AccountTransaction

interface AccountTransactionRepository : ReactiveMongoRepository<AccountTransaction, String> {
    fun findByReference(transactionReference: String): Mono<AccountTransaction>
}
