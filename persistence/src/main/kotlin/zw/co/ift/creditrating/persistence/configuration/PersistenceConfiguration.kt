package zw.co.ift.creditrating.persistence.configuration

import com.mongodb.reactivestreams.client.MongoClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@ComponentScan(basePackages = ["zw.co.ift.creditrating.persistence"])
@EnableReactiveMongoRepositories(basePackages = ["zw.co.ift.creditrating.persistence.repository"])
class PersistenceConfiguration : AbstractReactiveMongoConfiguration() {
    @Bean
    override fun reactiveMongoClient() = MongoClients.create()

    override fun getDatabaseName() = "bank"

    @Bean
    override fun reactiveMongoTemplate() = ReactiveMongoTemplate(reactiveMongoClient(), databaseName)
}