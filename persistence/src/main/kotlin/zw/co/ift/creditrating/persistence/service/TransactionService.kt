package zw.co.ift.creditrating.persistence.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.model.AccountTransaction
import zw.co.ift.creditrating.persistence.repository.AccountTransactionRepository
import reactor.core.publisher.Flux
import zw.co.ift.creditrating.persistence.model.TransactionState
import zw.co.ift.creditrating.utilities.generateTransactionReference


interface TransactionService {
    fun findByReference(transactionReference: String): Mono<AccountTransaction>
    fun processTransaction(pendingTransaction: AccountTransaction): Mono<AccountTransaction>
}

@Service
class TransactionServiceImpl(private val repository: AccountTransactionRepository, private val accountService: AccountService) : TransactionService {
    val log: Logger = LoggerFactory.getLogger(TransactionServiceImpl::class.java)

    override fun processTransaction(pendingTransaction: AccountTransaction): Mono<AccountTransaction> {
        log.info("Processing ${pendingTransaction.transactionCategory} :: Source Account : ${pendingTransaction.sourceAccount} |  Destination Account : ${pendingTransaction.destinationAccount} |  Amount : $${pendingTransaction.amount} ")
        val sourceAccountMono = accountService.findByAccountNumber(pendingTransaction.sourceAccount)
        val destinationAccountMono = accountService.findByAccountNumber(pendingTransaction.destinationAccount)

        return Mono.zip(sourceAccountMono, destinationAccountMono)
                .map({ element ->
                    effectTransfer(element.t1, element.t2, pendingTransaction)
                })
                .flatMap { processedTransaction ->
                    repository.save(processedTransaction) }
                .switchIfEmpty(Mono.empty())
    }

    private fun effectTransfer(source: Account, destination: Account, pendingTransaction: AccountTransaction): AccountTransaction {
        val reference = generateTransactionReference()
        pendingTransaction.reference = reference
        log.info("Processing transaction $reference...")
        if (source.currentBalance >= pendingTransaction.amount) {
            source.currentBalance = source.currentBalance.subtract(pendingTransaction.amount)
            destination.currentBalance = destination.currentBalance.add(pendingTransaction.amount)
            accountService.update(source).block()
            accountService.update(destination).block()
            pendingTransaction.state = TransactionState.SUCCESSFUL
            pendingTransaction.summary = "Transferred $${pendingTransaction.amount} from ${source.accountNumber} to ${destination.accountNumber}"
            log.info(pendingTransaction.summary)
        } else {
            pendingTransaction.state = TransactionState.FAILED
            pendingTransaction.summary = "${source.accountNumber} has insufficient funds [Current Balance: $${source.currentBalance} :: Transaction Amount : $${pendingTransaction.amount}"
        }
        return pendingTransaction
    }

    override fun findByReference(transactionReference: String): Mono<AccountTransaction> = repository.findByReference(transactionReference)

}