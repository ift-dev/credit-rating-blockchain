package zw.co.ift.creditrating.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime

@Document
data class AccountTransaction(
        @Id
        var id: String? = null,
        @Indexed
        var sourceAccount: String,
        @Indexed
        var destinationAccount: String,
        var transactionCategory: String,
        var amount: BigDecimal = BigDecimal.ZERO,
        @Indexed(unique = true)
        var reference: String? = null,
        var state: TransactionState? = null,
        @Indexed
        var transactionTime: LocalDateTime = LocalDateTime.now(),
        var description: String,
        var summary: String? = null
)

