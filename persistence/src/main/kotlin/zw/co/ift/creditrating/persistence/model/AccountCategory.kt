package zw.co.ift.creditrating.persistence.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal

/**
 * used to classify the different types of accounts in the system
 */
@Document
data class AccountCategory(
        @Id
        var id: String,
        var name: String,
        var maximumBalance: BigDecimal,
        var minimumBalance: BigDecimal
)