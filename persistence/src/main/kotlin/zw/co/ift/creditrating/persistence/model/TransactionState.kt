package zw.co.ift.creditrating.persistence.model

enum class TransactionState{
    COMPLETED, PENDING, PROCESSING, FAILED, SUCCESSFUL
}