package zw.co.ift.creditrating.persistence.configuration

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["zw.co.ift.creditrating.persistence"])
class PersistenceIntegrationConfiguration