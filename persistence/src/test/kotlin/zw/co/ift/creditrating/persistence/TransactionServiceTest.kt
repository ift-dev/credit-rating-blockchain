package zw.co.ift.creditrating.persistence

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import reactor.core.publisher.Flux
import zw.co.ift.creditrating.persistence.configuration.PersistenceIntegrationConfiguration
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.model.AccountCategory
import zw.co.ift.creditrating.persistence.model.AccountTransaction
import zw.co.ift.creditrating.persistence.model.TransactionState
import zw.co.ift.creditrating.persistence.repository.AccountRepository
import zw.co.ift.creditrating.persistence.repository.AccountTransactionRepository
import zw.co.ift.creditrating.persistence.service.TransactionService
import java.math.BigDecimal
import java.time.LocalDateTime

@DataMongoTest
@ContextConfiguration(classes = [(PersistenceIntegrationConfiguration::class)])
@DirtiesContext
@RunWith(SpringRunner::class)
class TransactionServiceTest {

    private val log: Logger = LoggerFactory.getLogger(TransactionServiceTest::class.java)
    private val sampleTransactionId = "TRANS.0001"
    private val savingsAccount = AccountCategory(id = "1", maximumBalance = BigDecimal.TEN, minimumBalance = BigDecimal.ONE, name = "Savings Account")
    private val sampleSourceAccount = Account(accountName = "Developer Account0", accountNumber = "ACC.0001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.valueOf(1000))
    private val sampleDestinationAccount = Account(accountName = "Developer Account1", accountNumber = "ACC.0002", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.valueOf(2000))

    @Autowired
    private lateinit var repository: AccountTransactionRepository
    @Autowired
    private lateinit var transactionService: TransactionService
    @Autowired
    private lateinit var accountRepository: AccountRepository

    @Before
    fun initializeDatabase() {
        log.info("Initializing test data ...")
        val accounts = Flux.just(sampleSourceAccount, sampleDestinationAccount).log()
        accountRepository.saveAll(accounts).subscribe()
        val sampleTransaction = AccountTransaction(sourceAccount = "ACC.00001", destinationAccount = "ACC.00002", amount = BigDecimal.ONE, reference = sampleTransactionId, state = TransactionState.COMPLETED, transactionTime = LocalDateTime.now(), transactionCategory = "TRANSFER", description = "Transfer from one account to another")
        val transactions = Flux.just(sampleTransaction).log()
        repository.saveAll(transactions).subscribe()
    }

    @Test
    fun `transfer funds from one account to another`() {
        log.info("Processing funds transfer from one account to another ...")
        val sourceAccountBefore = accountRepository.findByAccountNumber(sampleSourceAccount.accountNumber).block()
        val destinationAccountBefore = accountRepository.findByAccountNumber(sampleDestinationAccount.accountNumber).block()
        val transaction = AccountTransaction(sourceAccount = sourceAccountBefore.accountNumber, destinationAccount = destinationAccountBefore.accountNumber, amount = BigDecimal.TEN, transactionCategory = "TRANSFER", description = "Transfer of $${BigDecimal.TEN} from ${sourceAccountBefore.accountNumber} to ${destinationAccountBefore.accountNumber}")
        val transactionResult = transactionService.processTransaction(transaction).block()
        val sourceAccountAfter = accountRepository.findByAccountNumber(sampleSourceAccount.accountNumber).block()
        val destinationAccountAfter = accountRepository.findByAccountNumber(sampleDestinationAccount.accountNumber).block()
        assertThat(transactionResult.state).isEqualTo(TransactionState.SUCCESSFUL)
        assertThat(sourceAccountAfter.currentBalance).isEqualTo(BigDecimal.valueOf(990))
        assertThat(destinationAccountAfter.currentBalance).isEqualTo(BigDecimal.valueOf(2010))
        log.info("Processed transaction $transactionResult")
    }

    @Test
    fun `insufficient funds transfer`() {
        log.info("Testing insufficient funds transfer processing ...")
        val sourceAccountBefore = accountRepository.findByAccountNumber(sampleSourceAccount.accountNumber).block()
        val destinationAccountBefore = accountRepository.findByAccountNumber(sampleDestinationAccount.accountNumber).block()
        val transaction = AccountTransaction(sourceAccount = sourceAccountBefore.accountNumber, destinationAccount = destinationAccountBefore.accountNumber, amount = BigDecimal.valueOf(10000), transactionCategory = "TRANSFER", description = "Transfer of $${BigDecimal.TEN} from ${sourceAccountBefore.accountNumber} to ${destinationAccountBefore.accountNumber}")
        val transactionResult = transactionService.processTransaction(transaction).block()
        val sourceAccountAfter = accountRepository.findByAccountNumber(sampleSourceAccount.accountNumber).block()
        val destinationAccountAfter = accountRepository.findByAccountNumber(sampleDestinationAccount.accountNumber).block()
        assertThat(transactionResult.state).isEqualTo(TransactionState.FAILED)
        assertThat(sourceAccountAfter.currentBalance).isEqualTo(BigDecimal.valueOf(1000))
        assertThat(destinationAccountAfter.currentBalance).isEqualTo(BigDecimal.valueOf(2000))
        log.info("Processed transaction $transactionResult")
    }

    @Test
    fun `invalid source account for funds transfer`() {
        log.info("Processing funds transfer from one account to another ...")
        val transaction = AccountTransaction(sourceAccount = "ACC.00010", destinationAccount = "ACC.0002", amount = BigDecimal.valueOf(10000), transactionCategory = "TRANSFER", description = "Transfer of $${BigDecimal.TEN} from ACC.00010 to ACC.00020")
        val transactionResult = transactionService.processTransaction(transaction).block()
        assertThat(transactionResult).isNull()
        log.info("Processed transaction $transactionResult")
    }

    @Test
    fun `invalid destination account for funds transfer`() {
        log.info("Processing funds transfer from one account to another ...")
        val transaction = AccountTransaction(sourceAccount = "ACC.0001", destinationAccount = "ACC.00020", amount = BigDecimal.valueOf(10000), transactionCategory = "TRANSFER", description = "Transfer of $${BigDecimal.TEN} from ACC.00010 to ACC.00020")
        val transactionResult = transactionService.processTransaction(transaction).block()
        assertThat(transactionResult).isNull()
        log.info("Processed transaction $transactionResult")
    }

    @Test
    fun `invalid accounts for funds transfer`() {
        log.info("Processing funds transfer from one account to another ...")
        val transaction = AccountTransaction(sourceAccount = "ACC.00010", destinationAccount = "ACC.00020", amount = BigDecimal.valueOf(10000), transactionCategory = "TRANSFER", description = "Transfer of $${BigDecimal.TEN} from ACC.00010 to ACC.00020")
        val transactionResult = transactionService.processTransaction(transaction).block()
        assertThat(transactionResult).isNull()
        log.info("Processed transaction $transactionResult")
    }

    @Test
    fun `search for a transaction given a valid reference`() {
        log.info("Searching for an existing transaction with id TRANS.0001")
        val transaction = transactionService.findByReference(sampleTransactionId).block()
        log.info("Retrieved :: $transaction")
        assertThat(transaction.id).isNotNull()
        assertThat(transaction.sourceAccount).isEqualTo("ACC.00001")
    }

}