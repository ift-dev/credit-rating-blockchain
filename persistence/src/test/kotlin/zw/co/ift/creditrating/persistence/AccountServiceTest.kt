package zw.co.ift.creditrating.persistence

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import reactor.core.publisher.Flux
import zw.co.ift.creditrating.persistence.configuration.PersistenceIntegrationConfiguration
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.model.AccountCategory
import zw.co.ift.creditrating.persistence.repository.AccountRepository
import zw.co.ift.creditrating.persistence.service.AccountService
import java.math.BigDecimal

@DataMongoTest
@ContextConfiguration(classes = [(PersistenceIntegrationConfiguration::class)])
@DirtiesContext
@RunWith(SpringRunner::class)
class AccountServiceTest {
    private val log: Logger = LoggerFactory.getLogger(AccountServiceTest::class.java)

    @Autowired
    private lateinit var repository: AccountRepository
    @Autowired
    private lateinit var accountService: AccountService

    private val savingsAccount = AccountCategory(id = "1", maximumBalance = BigDecimal.TEN, minimumBalance = BigDecimal.ONE, name = "Savings Account")
    private val sampleAccount = Account(accountName = "Developer Account", accountNumber = "ACC.0001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE)
    private val sampleAccountTest = Account(accountName = "Developer", accountNumber = "00001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE)


    @Before
    fun initializeDatabase() {
        val accounts = Flux.just(sampleAccount, sampleAccountTest).log()
        repository.saveAll(accounts).subscribe()
    }

    @Test
    fun `find account by account number`() {
        log.info("Searching for account with accountNumber: 1")
        val accountMono = accountService.findByAccountNumber(sampleAccount.accountNumber).log()
        val accountDetails = accountMono.block()
        log.info("Retrieved account :: $accountDetails")
        assertThat(accountMono).isNotNull()
        assertThat(accountDetails.id).isNotNull()
        assertThat(accountDetails!!.accountName).isEqualToIgnoringCase(sampleAccount.accountName)
    }

    @Test
    fun `create an existing account`() {
        log.info("Create an already existing account..")
        val existingAccount = accountService.findByAccountNumber(sampleAccount.accountNumber).block()
        val createdAccount = accountService.createAccount(sampleAccount).block()
        log.info("Retrieved account :: $existingAccount")
        log.info("Retrieved account :: $createdAccount")
        assertThat(createdAccount!!.id).isNotNull()
        assertThat(createdAccount.id).isEqualToIgnoringCase(existingAccount!!.id)
        assertThat(createdAccount.accountNumber).isEqualToIgnoringCase(existingAccount.accountNumber)
    }

    @Test
    fun `create a new account`() {
        log.info("Creating an account...")
        val newAccount = Account(accountName = "Developer Account", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE)
        val createdAccountMono = accountService.createAccount(newAccount)
        val createdAccount = createdAccountMono.block()
        log.info("Returned response :: $createdAccount")
        assertThat(createdAccount).isNotNull()
        assertThat(createdAccount!!.accountName).isEqualToIgnoringCase("Developer Account")
        assertThat(createdAccount.accountNumber).isNotNull()
    }

}