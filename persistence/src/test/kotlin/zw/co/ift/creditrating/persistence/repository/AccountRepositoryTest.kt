package zw.co.ift.creditrating.persistence.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import reactor.core.publisher.Flux
import reactor.test.StepVerifier
import zw.co.ift.creditrating.persistence.configuration.PersistenceIntegrationConfiguration
import zw.co.ift.creditrating.persistence.model.Account
import zw.co.ift.creditrating.persistence.model.AccountCategory
import java.math.BigDecimal


/**
 * Tests the completeness of the repository makes use of Mongo Embedded
 */

@DataMongoTest
@ContextConfiguration(classes = [(PersistenceIntegrationConfiguration::class)])
@DirtiesContext
@RunWith(SpringRunner::class)
class AccountRepositoryTest {
    private val log: Logger = LoggerFactory.getLogger(AccountRepositoryTest::class.java)
    @Autowired
    private lateinit var repository: AccountRepository
    private val savingsAccount = AccountCategory(id = "1", maximumBalance = BigDecimal.TEN, minimumBalance = BigDecimal.ONE, name = "Savings Account")

    private val sampleAccount = Account(accountName = "Developer Account0", accountNumber = "ACC.0001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE)

    @Before
    fun initializeDatabase() {
        val savingsAccount = AccountCategory(id = "1", maximumBalance = BigDecimal.TEN, minimumBalance = BigDecimal.ONE, name = "Savings Account")
        val accounts = Flux.just(
                Account(accountName = "Developer Account0", accountNumber = "ACC.0001", accountCategory = savingsAccount, accountState = "Active", currentBalance = BigDecimal.ONE))
                .log()
        repository.saveAll(accounts).subscribe()
    }

    @Test
    fun `get an account given a valid accountNumber`() {
        val mono = repository.findByAccountNumber("ACC.0001").log()
        log.info("Searching for accountNumber ACC.0001")
        assertThat(mono).isNotNull()
        val account = mono.block()
        log.info("Retrieved account $account")
        assertThat(account!!.accountName).isEqualToIgnoringCase("Developer Account0")
    }

    @Test
    fun `return an error when we cannot find an account`() {
        val mono = repository.findByAccountNumber("12345")
        log.info("Searching for account number 12345. Response $mono")
        StepVerifier.create(mono)
                .expectComplete()
                .log()
                .verify()
        assertThat(mono.toString()).isEqualToIgnoringCase("MonoOnErrorResume")
    }
}